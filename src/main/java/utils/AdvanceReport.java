package utils;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class AdvanceReport {

	static ExtentHtmlReporter html;
	static ExtentReports extent;
	ExtentTest test;
	@BeforeSuite
	public void startReport() {
		html = new ExtentHtmlReporter("./report/extentReport.html");
		extent = new ExtentReports();
	    html.setAppendExisting(true); 
		extent.attachReporter(html); 
	}
	@BeforeMethod
	public void initializeTest(String testcaseName,String testDec, String author, String category) {
	    test = extent.createTest(testcaseName, testDec);
	    test.assignAuthor(author);
	    test.assignCategory(category);
	   
	}
	@AfterTest
	public void logStep(String des, String status) {
		if (status.equalsIgnoreCase("pass")) {
			test.pass(des);
		} else if (status.equalsIgnoreCase("fail")) {
			test.fail(des);
			throw new RuntimeException();
		} else if (status.equalsIgnoreCase("warning")) {
			test.warning(des);
		}
	
	}
	@AfterSuite
	public void endReport() {
		extent.flush();
	}
}
