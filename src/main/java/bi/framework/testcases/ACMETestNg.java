package bi.framework.testcases;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class ACMETestNg extends SeleniumBase {

	@Test
	public void findVendorNm() {
		startApp("chrome", "https://acme-test.uipath.com/account/login");
		WebElement email = locateElement("xpath", "(//div[@class='controls'])[1]//input");
		clearAndType(email, "rakuqa@gmail.com");
		WebElement passWord = locateElement("xpath", "(//div[@class='controls'])[2]//input");
		clearAndType(passWord, "9876543210");
		WebElement loginBtn = locateElement("id", "buttonLogin");
		click(loginBtn);
		WebElement vendor = locateElement("xpath", "(//button[@class='btn btn-default btn-lg'])[4]");
		Actions act= new Actions(driver);
		act.moveToElement(vendor).perform();
		WebElement SrchVendor = locateElement("LinkText", "Search for Vendor");
		click(SrchVendor);
		WebElement vendorID = locateElement("id", "vendorTaxID");
		clearAndType(vendorID, "DE767565");
		WebElement vendorSrch = locateElement("id", "buttonSearch");
		click(vendorSrch);
		List<WebElement> tableRow = locateElements("TagName","tr");
		WebElement firstTableRow = tableRow.get(1);
		List<WebElement> td = firstTableRow.findElements(By.tagName("td"));
		System.out.println(td.get(0).getText());
	}
}
