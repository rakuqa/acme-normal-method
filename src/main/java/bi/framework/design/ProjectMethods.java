package bi.framework.design;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class ProjectMethods extends SeleniumBase {

	@BeforeMethod
	public void login() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement CrmSfa = locateElement("LinkText", "CRM/SFA");
		click(CrmSfa);
		System.out.println("Clicked " + CrmSfa + " Succesfully");
	}
}
