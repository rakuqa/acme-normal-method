package bl.framework.api;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import bl.framework.base.Browser;
import bl.framework.base.Element;

public class SeleniumBase implements Browser, Element{

	public RemoteWebDriver driver;
	public int i =1;
	@Override
	public void startApp(String url) {
		// TODO Auto-generated method stub

	}

	@Override
	public void startApp(String browser, String url) {
		if(browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver();
		} else if(browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
			driver = new FirefoxDriver(); 
		}
		driver.manage().window().maximize();
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("The browser "+browser+" launched successfully");
        takeSnap();
	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		switch (locatorType) {
		case "id": return driver.findElementById(value);
		case "name": return driver.findElementByName(value);
		case "class": return driver.findElementByClassName(value);
		case "xpath": return driver.findElementByXPath(value);
		case "LinkText": return driver.findElementByLinkText(value);
		case "TagName": return driver.findElementByTagName(value);
		default:
			break;
		}
		return null;
	}

	@Override
	public WebElement locateElement(String value) {
		// TODO Auto-generated method stub
		switch (value) {
		case "id": return driver.findElementById(value);
		case "name": return driver.findElementByName(value);
		case "class": return driver.findElementByClassName(value);
		case "xpath": return driver.findElementByXPath(value);
		case "LinkText": return driver.findElementByLinkText(value);
		case "PartialLinkText" : return driver.findElementByPartialLinkText(value);
		default:
			break;
		}
		
		return null;
	}

	@Override
	public List<WebElement> locateElements(String locatorType, String value) {
		switch (locatorType){
	case "id": return driver.findElementsById(value);
	case "name": return driver.findElementsByName(value);
	case "class": return driver.findElementsByClassName(value);
	case "xpath": return driver.findElementsByXPath(value);
	case "LinkText": return driver.findElementsByLinkText(value);
	case "TagName": return driver.findElementsByTagName(value);
	default:
		break;
	}
	return null;
	}

	@Override
	public void switchToAlert() {
		// TODO Auto-generated method stub
		driver.switchTo().alert();
	}

	@Override
	public void acceptAlert() {
		// TODO Auto-generated method stub
		driver.switchTo().alert().accept();
	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		driver.switchTo().alert().dismiss();
	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		driver.switchTo().alert().getText();
		return null;
	}

	@Override
	public void typeAlert(String data) {
		// TODO Auto-generated method stub
		driver.switchTo().alert().sendKeys(data);
	}

	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub
		Set<String> windowHandles = driver.getWindowHandles();
		List <String>ls= new ArrayList<String>();
		ls.addAll(windowHandles);
		driver.switchTo().window(ls.get(index));
	}

	@Override
	public void switchToWindow(String title) {
		// TODO Auto-generated method stub
		Set<String> windowHandles = driver.getWindowHandles();
		ArrayList <String> ls= new ArrayList<String>();
		ls.addAll(windowHandles);
		driver.switchTo().window(ls.get(i));
		System.out.println(driver.getTitle());
		driver.switchTo().window(title);
	}

	@Override
	public void switchToFrame(int index) {
		// TODO Auto-generated method stub
		driver.switchTo().frame(index);
	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub
		
		driver.switchTo().frame(ele);
	}

	@Override
	public void switchToFrame(String idOrName) {
		// TODO Auto-generated method stub
		driver.switchTo().frame(idOrName);
	}

	@Override
	public void defaultContent() {
		// TODO Auto-generated method stub
		driver.switchTo().defaultContent();
	}

	@Override
	public boolean verifyUrl(String url) {
		// TODO Auto-generated method stub
		String currentUrl = driver.getCurrentUrl();
		Assert.assertEquals(currentUrl, url);
		return false;
	}

	@Override
	public boolean verifyTitle(String title) {
		// TODO Auto-generated method stub
		String Verifytitle = driver.getTitle();
		Assert.assertEquals(Verifytitle, title);
		return false;
	}

	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		driver.close();
	}

	@Override
	public void quit() {
		// TODO Auto-generated method stub
		driver.quit();
	}

	@Override
	public void click(WebElement ele) {
		ele.click();
		System.out.println("The element "+ele+" clicked successfully");
		takeSnap();
		
	}

	@Override
	public void append(WebElement ele, String data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clear(WebElement ele) {
		// TODO Auto-generated method stub
		ele.clear();
		System.out.println("The Element "+ele+" is cleared");
		takeSnap();
	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		ele.clear();
		ele.sendKeys(data); 
		System.out.println("The data "+data+" entered successfully");
		takeSnap();
	}

	@Override
	public String getElementText(WebElement ele) {
		// TODO Auto-generated method stub
		ele.getText();
		return null;
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTypedText(WebElement ele) {
		// TODO Auto-generated method stub
		ele.getText();
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		// TODO Auto-generated method stub
		Select sc = new Select(ele);
		sc.selectByVisibleText(value);
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub
		Select sc = new Select(ele);
		sc.selectByIndex(index);
	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		// TODO Auto-generated method stub
		Select sc = new Select(ele);
		sc.selectByValue(value);
	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		String text1 = ele.getText();
		Assert.assertEquals(text1, expectedText);
		return false;
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		String text = ele.getText();
		//
		Assert.assertEquals(text, expectedText);
		return false;
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		ele.isDisplayed();
		//
		return false;
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

}
