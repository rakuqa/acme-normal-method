package acmeNormal;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class ACME_NormalMethod {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://acme-test.uipath.com/account/login");
		System.out.println(driver.getTitle());
		driver.findElementByXPath("(//div[@class='controls'])[1]//input").sendKeys("rakuqa@gmail.com");
		driver.findElementByXPath("(//div[@class='controls'])[2]//input").sendKeys("9876543210");
		driver.findElementById("buttonLogin").click();
		Actions act = new Actions(driver);
		WebElement vendor = driver.findElementByXPath("(//button[@class='btn btn-default btn-lg'])[4]");
		// act.moveToElement(vendor).click();
		act.moveToElement(vendor).perform();
		driver.findElementByLinkText("Search for Vendor").click();

		driver.findElementById("vendorTaxID").sendKeys("DE767565");
		driver.findElementById("buttonSearch").click();
		List<WebElement> listtr = driver.findElementsByTagName("tr");
		System.out.println(listtr.size());

		WebElement firsttr = listtr.get(1);
		// System.out.println(firsttr);
		List<WebElement> td = firsttr.findElements(By.tagName("td"));
		System.out.println(td.get(0).getText());

	}

}
